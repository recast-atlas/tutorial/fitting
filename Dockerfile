FROM atlasamglab/stats-base:root6.22.06

COPY . /code

WORKDIR /code

RUN python -m pip --no-cache-dir install --upgrade pip setuptools wheel && \
    python -m pip --no-cache-dir install --requirement requirements.txt && \
    python -m pip list
