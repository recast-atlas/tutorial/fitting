"""
  Author:   Lukas Heinrich
  Purpose:  Perform fit toy data to the input signal plus mock background generated from a falling exponential pdf. Report the fit result.
  Revision History:
        - 190812 (Danika MacDonell):
            * create mock background and data on the spot, rather than reading them in, for transparency
            * do scaling in this step, rather than previous
            * read in signal as txt rather than json file (to jive with preliminary tutorial exercise)
            * plot data, background, & signal
            * plot mu scan rather than outputting limit as json

  Sample run command: python run_fit.py -i selected.txt -o limit.png -p hists.png -c 44.837 -s 6813.025800 -k 1.0 -f 1.0 -l 140.1
"""

import argparse
import math

import matplotlib.pyplot as plt
import numpy as np
import pyhf
import ROOT

plt.rc("xtick", labelsize=16)
plt.rc("ytick", labelsize=16)


def main():

    parser = argparse.ArgumentParser()
    parser.add_argument("--filedata", type=str, help="")
    parser.add_argument("--histdata", type=str, help="")
    parser.add_argument("--filebkg", type=str, help="")
    parser.add_argument("--histbkg", type=str, help="")
    parser.add_argument("--filesig", type=str, help="")
    parser.add_argument("--histsig", type=str, help="")
    parser.add_argument("--outputfile", type=str, help="")
    parser.add_argument("--plotfile", type=str, help="")

    args = parser.parse_args()
    filedata = args.filedata
    histdata = args.histdata
    filebkg = args.filebkg
    histbkg = args.histbkg
    filesig = args.filesig
    histsig = args.histsig
    outputfile = args.outputfile
    plotfile = args.plotfile

    print("filedata   : ", filedata)
    print("histdata   : ", histdata)
    print("filebkg    : ", filebkg)
    print("histbkg    : ", histbkg)
    print("filesig    : ", filesig)
    print("histsig    : ", histsig)
    print("outputfile : ", outputfile)
    print("plotfile   : ", plotfile)

    # collect signal info
    f_signal = ROOT.TFile(filesig)
    h_signal = f_signal.Get(histsig)

    sig = []
    sig_edges = []

    for ibin in range(1, h_signal.GetNbinsX() + 2):
        sig_edges.append(h_signal.GetBinLowEdge(ibin))
        sig.append(h_signal.GetBinContent(ibin))

    sig_str = ""
    sig_edges_str = ""
    for i in range(len(sig)):
        sig_str += str(sig[i])
        sig_str += "  "
        sig_edges_str += str(sig_edges[i])
        sig_edges_str += "  "

    edges = np.fromstring(sig_edges_str, dtype=float, sep=" ")
    signal = np.fromstring(sig_str, dtype=float, sep=" ")
    bin_width = edges[1] - edges[0]

    print(edges)
    print(signal)

    # collect background info
    f_background = ROOT.TFile(filebkg)
    h_background = f_background.Get(histbkg)

    bkg = []
    bkg_uncer = []

    for ibin in range(1, h_background.GetNbinsX() + 2):
        bkg.append(h_background.GetBinContent(ibin))
        bkg_uncer.append(h_background.GetBinError(ibin))

    bkg_str = ""
    bkg_uncer_str = ""
    for i in range(len(bkg)):
        bkg_str += str(bkg[i])
        bkg_str += "  "
        bkg_uncer_str += str(bkg_uncer[i])
        bkg_uncer_str += "  "

    background = np.fromstring(bkg_str, dtype=float, sep=" ")
    background_uncert = np.fromstring(bkg_uncer_str, dtype=float, sep=" ")
    bin_width = edges[1] - edges[0]

    # collect background info
    f_data = ROOT.TFile(filedata)
    h_data = f_data.Get(histdata)

    dat = []

    for ibin in range(1, h_data.GetNbinsX() + 2):
        dat.append(h_data.GetBinContent(ibin))

    dat_str = ""
    for i in range(len(dat)):
        dat_str += str(dat[i])
        dat_str += "  "

    data = np.fromstring(dat_str, dtype=float, sep=" ")
    bin_width = edges[1] - edges[0]

    print(signal)
    print(background)
    print(edges)

    print(len(signal))
    print(len(background))
    print(len(edges))

    # Plot the background, signal, and data
    print(type(background))
    sum_bkg = sum(background)
    sum_sig = sum(signal)

    scaleFactor = int(math.ceil(((sum_bkg / sum_sig) * 0.05) / 10.0) * 10)

    print(signal)
    signal = signal * scaleFactor
    print(signal)

    plt.bar(
        edges,
        signal + background,
        width=bin_width,
        facecolor="red",
        label="sig*(" + str(scaleFactor) + ")+bkg",
    )
    plt.bar(edges, background, width=bin_width, facecolor="steelblue", label="bkg")
    plt.errorbar(
        edges,
        data,
        c="k",
        zorder=10,
        yerr=np.sqrt(data),
        fmt="o",
        label="data",
        markersize=3,
    )
    plt.legend(prop={"size": 16})
    plt.xlabel("Dijet Invariant Mass (GeV)", fontsize=16)
    plt.ylabel("Events / %.0f GeV" % (edges[1] - edges[0]), fontsize=16)
    plt.tight_layout()
    plt.savefig(plotfile)
    plt.close()

    # unscale signal
    signal = signal / scaleFactor

    # build likelihood
    pdf = pyhf.simplemodels.hepdata_like(
        signal.tolist(),
        background.tolist(),
        background_uncert.tolist(),
    )
    obs, expset = pyhf.infer.hypotest(
        1.0, data.tolist() + pdf.config.auxdata, pdf, return_expected_set=True
    )

    results = []
    muset = np.linspace(0.01, 9.99, 20)
    for mu in muset:
        print("Testing mu : ", mu)
        obs, exp = pyhf.infer.hypotest(
            mu, data.tolist() + pdf.config.auxdata, pdf, return_expected_set=True
        )
        results.append((obs, exp))

    print(results)

    observed = [r[0] for r in results]
    down_2sig = [r[1][0] for r in results]
    up_2sig = [r[1][-1] for r in results]
    down_1sig = [r[1][1] for r in results]
    up_1sig = [r[1][-2] for r in results]
    expected = [r[1][2] for r in results]

    fig = plt.figure(figsize=(7, 5))
    ax = fig.add_subplot(1, 1, 1)
    ax.set_title("Hypothesis Tests", fontsize=16)
    ax.set_ylabel("CL$_s$", fontsize=16)
    ax.set_xlabel(r"$\mu$", fontsize=16)
    plt.tight_layout()
    ax.plot(muset, observed, c="black", label="observed")
    ax.plot(muset, expected, c="cyan", ls="dashed", label="expected")
    ax.fill_between(
        muset, down_2sig, up_2sig, facecolor="yellow", label=r"expected $\pm$ 2$\sigma$"
    )
    ax.fill_between(
        muset, down_1sig, up_1sig, facecolor="green", label=r"expected $\pm$ 1$\sigma$"
    )
    ax.axhline(0.05, c="red")
    ax.set_xlim(0, 10.0)
    ax.legend(prop={"size": 16})
    plt.savefig(outputfile)

    """
  result = {
      'obs': obs.tolist()[0],
      'exp': [e.tolist()[0] for e in expset]
  }
  result = json.dumps(result, indent = 4)
  click.secho(result)
  with open(outputfile,'w') as f:
      f.write(result)
  """


if __name__ == "__main__":
    main()
